//Use the require directive to load Node.js Modules
//module is a software component or part of a program that contains one or more routines
//http module: - let node js transfer data using the hypertext transfer protocol. It is a set of individual files that contain code to create a component that helps established data transfer between applications
let http = require("http");
//clients (browser) and servers (node.js/express js application) communicate by exchanging individual messages.
//ex. message sent by the client, usually a web browser are called requests.
//http://home
//message sent now by the server as an answer are called responses.


//createServer() method - used to create an http server that listens to requests on a speciafied port and gives responses back to the client.

//it accepts a function and allows us to perform a certain task for our server. 
http.createServer(function(request, response){

//Use the writeHead() method to:
//Set a status code for the response - 200 means OK(Successfull)
//Set the content-type of the response as a plain text message
	response.writeHead(200, {"content-type": "type/text"});
	response.end("Goodbye!")


}).listen(4000)
//A port is a virtual point where network connection start and end.
//Each port is associated with a specific process or service.
//The server will be assign to port 4000 via the "listen(4000)" method where the server will listen to any requests that are sent to our server.


//When a server is running, console will print this message:
console.log("server running at localhost:4000")


//http://localhost:4000













const http = require("http");

//Create a variable "port" to store the port number.
const port = 4000;

//Create a variable "server" that stores the uotput of the create server method.

const server = http.createServer((req, res) => {
//conditional statement
//https://localhost:4000/greeting
if(req.url == "/greeting"){
	res.writeHead(200, {"content-type": "text/plain"})
	res.end("This is the homepage")

}else if(req.url == "/home"){
	res.writeHead(200, {"content-type": "text/plain"})
	res.end("This is the homepage") 
}

else{
	res.writeHead(404, {"content-type": "text/plain"})
	res.end("Page not found")
	}
})

//Use the server and port variables created above.

server.listen(port);

console.log(`server now accessible from localhost:${port}`);

//http://localhost:4000/home/about/contacts - tawag na natin sa kanya routes



